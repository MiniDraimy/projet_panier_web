<?php

namespace App\Repository;

use App\Entity\LigneCommande;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LigneCommande|null find($id, $lockMode = null, $lockVersion = null)
 * @method LigneCommande|null findOneBy(array $criteria, array $orderBy = null)
 * @method LigneCommande[]    findAll()
 * @method LigneCommande[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LigneCommandeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LigneCommande::class);
    }

    public function findCommandeByID(int $id)
    {
        return $this->createQueryBuilder('ligneCommande')
            ->where('ligneCommande.commande=?1')
            ->setParameter(1,$id)
            ->getQuery()
            ->getResult();
    }

    public function total(int $id)
    {
        return $this->createQueryBuilder('ligneCommande')
            ->select('sum(ligneCommande.prix)')
            ->where('ligneCommande.commande=?1')
            ->setParameter(1,$id)
            ->getQuery()
            ->getResult();
    }

    public function CA() {
        return $this->createQueryBuilder('ligneCommande')
            ->select('sum(ligneCommande.prix)')
            ->getQuery()
            ->getResult();
    }

    public function moyenneParClient() {
        $conn = $this->getEntityManager()->getConnection();
        $requete ="select sum(ligne_commande.prix)/count(commande.user_id) as moy from ligne_commande join commande on ligne_commande.commande_id = commande.id";
        $prep=$conn->prepare($requete);
        $prep->execute();
        $results = $prep->fetchAll();
        return $results;
//        return $this->createQueryBuilder('ligneCommande')
//            ->select('sum(ligneCommande.prix)/count(commande.user)')
//            ->from('ligneCommande')
//            ->join('commande','commande','ligneCommande.commande = commande.id')
//            ->getQuery()
//            ->getResult();
    }

    public function NBcommandeTotal($before, $after) {
        $conn = $this->getEntityManager()->getConnection();
        $requete ="select count(commande.id) as cmd from commande where commande.date between '".$before."' and '".$after."'";
        $prep=$conn->prepare($requete);
        $prep->execute();
        $results = $prep->fetchAll();
        return $results;
    }

    public function moyenneParType() {
        $conn = $this->getEntityManager()->getConnection();
        $requete ="select count(ligne_commande.produit_id) as nbAchat, t.libelle as libelle, sum(ligne_commande.prix) as prix from ligne_commande join produit on ligne_commande.produit_id = produit.id join type_produit t on produit.type_produit_id = t.id group by t.libelle;";
        $prep=$conn->prepare($requete);
        $prep->execute();
        $results = $prep->fetchAll();
        return $results;

//        return $this->createQueryBuilder('ligneCommande')
//            ->select('count(c.produit) as nbAchat, t.libelle as libelle, sum(c.prix) as prix')
//            ->from('ligneCommande', 'c')
//            ->join('produit', 'p', 'ligneCommande.produit = p.id')
//            ->join('typeProduit','t', 'p.typeProduit = t.id')
//            ->groupBy('t.libelle')
//            ->getQuery()
//            ->getResult();
    }

    // /**
    //  * @return LigneCommande[] Returns an array of LigneCommande objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LigneCommande
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
