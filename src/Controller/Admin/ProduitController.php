<?php

namespace App\Controller\Admin;

use App\Entity\Commentaire;
use App\Service\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;    // objet REQUEST
use Symfony\Component\HttpFoundation\Response;    // objet RESPONSE

use App\Entity\Produit;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;

use App\Form\ProduitType;

/**
 * @Route(name="admin_", path="/admin")
 */

class ProduitController extends AbstractController
{

    private $manager;
    private $fileUploader;

    public function __construct(EntityManagerInterface $manager, FileUploader $fileUploader)
    {
        $this->manager = $manager;
        $this->fileUploader = $fileUploader;
    }
     /**
     * @Route("/", name="produit_index", methods={"GET"})
     */
    public function index()
    {
        return $this->redirectToRoute('admin_produit_show');
    }
    /**
     * @Route("/produit/show", name="produit_show", methods={"GET"})
     */
    public function showProduits()
    {
        $produits = $this->getDoctrine()->getRepository(Produit::class)->findBy([],['typeProduit' => 'ASC','stock' =>'ASC']);
        return $this->render('admin/produit/showProduits.html.twig', ['produits' => $produits]);
    }

    /**
     * @Route("/produit/add", name="produit_add", methods={"GET","POST"})
     * @param Request $request
     * @IsGranted("ROLE_ADMIN")
     * @return RedirectResponse|Response
     */
    public function addProduit(Request $request)
    {
        $produit = new Produit();
        $form = $this->createForm(ProduitType::class, $produit);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $form->getData();
            $targetDirectory = $this->getParameter('produit_img');
            $this->fileUploader->setTargetDirectory($targetDirectory);
            $imageFile = $form->get('photo')->getData();
            if ($imageFile) {
                if ($produit->getPhoto()) {
                    unlink($targetDirectory . '/' . $produit->getPhoto());
                }
                $imagePath = $this->fileUploader->upload($imageFile);
                $produit->setPhoto($imagePath);
            }
            $this->getDoctrine()->getManager()->persist($produit);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('notice', 'Produit ' . $produit->getNom() . ' ajouté');
            return $this->redirectToRoute('admin_produit_show');
        }

        return $this->render('admin/produit/addProduit.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/produit/edit/{id}<\d+>", name="produit_edit",  methods={"GET","PUT"})
     * @param Request $request
     * @param null $id
     * @IsGranted("ROLE_ADMIN")
     * @return RedirectResponse|Response
     */
    public function editProduit(Request $request, $id=null)
    {
        $produit = $this->getDoctrine()->getRepository(Produit::class)->find($id);
        if (!$produit)  throw $this->createNotFoundException('No produit found for id '.$id);
        $form = $this->createForm(ProduitType::class, $produit, [
            'action' => $this->generateUrl('admin_produit_edit',['id'=>$id]),
            'method' => 'PUT',]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $form->getData();
            $targetDirectory = $this->getParameter('produit_img');
            $this->fileUploader->setTargetDirectory($targetDirectory);
            $imageFile = $form->get('photo')->getData();
            if ($imageFile) {
                if ($produit->getPhoto()) {
                    unlink($targetDirectory . '/' . $produit->getPhoto());
                }
                $imagePath = $this->fileUploader->upload($imageFile);
                $produit->setPhoto($imagePath);
            }
            $this->getDoctrine()->getManager()->persist($produit);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('notice', 'Version 3 : Produit ' . $produit->getNom() . ' ajouté');
            return $this->redirectToRoute('admin_produit_show');
        }

        return $this->render('admin/produit/editProduit.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/produit/delete", name="produit_delete", methods={"DELETE"})
     * @param Request $request
     * @IsGranted("ROLE_ADMIN")
     * @return RedirectResponse
     */
    public function deleteProduit(Request $request)
    {
        if(!$this->isCsrfTokenValid('produit_delete', $request->get('token'))) {
            throw new  InvalidCsrfTokenException('Invalid CSRF token formulaire produit');
        }

        $entityManager = $this->getDoctrine()->getManager();
        $id= $request->request->get('id');
        $produit = $entityManager->getRepository(Produit::class)->find($id);
        if($produit->getPhoto()) {
            unlink($this->getParameter('produit_img') .'/' . $produit->getPhoto());
        }

        if (!$produit)  throw $this->createNotFoundException('No produit found for id '.$id);

        $entityManager->remove($produit);
        $entityManager->flush();
        return $this->redirectToRoute('admin_produit_show');
    }

    /**
     * @Route("/produit/details", name="produit_details", methods={"GET"})
     */
    public function detailsProduit()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $typeProduits=$entityManager->getRepository(Produit::class)->getDetailsProduits();

        return $this->render('admin/produit/detailsTypeProduit.html.twig', ['typeProduits' => $typeProduits]);
    }
}
