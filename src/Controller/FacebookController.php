<?php


namespace App\Controller;

use App\Entity\User;
use App\Security\LoginFormAuthenticator;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use KnpU\OAuth2ClientBundle\Client\Provider\FacebookClient;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Provider\FacebookUser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class FacebookController extends AbstractController
{
    /**
     * Link to this controller to start the "connect" process
     *
     * @Route("/connect/facebook", name="connect_facebook_start")
     * @param ClientRegistry $clientRegistry
     * @return RedirectResponse
     */
    public function connectAction(ClientRegistry $clientRegistry)
    {
        // on Symfony 3.3 or lower, $clientRegistry = $this->get('knpu.oauth2.registry');

        // will redirect to Facebook!
        return $clientRegistry
            ->getClient('facebook') // key used in config/packages/knpu_oauth2_client.yaml
            ->redirect([
                'public_profile', 'email' // the scopes you want to access
            ]);
    }

    /**
     * After going to Facebook, you're redirected back here
     * because this is the "redirect_route" you configured
     * in config/packages/knpu_oauth2_client.yaml
     *
     * @Route("/connect/facebook/check", name="connect_facebook_check")
     * @param Request $request
     * @param ClientRegistry $clientRegistry
     * @param GuardAuthenticatorHandler $guard
     * @param LoginFormAuthenticator $login
     * @return RedirectResponse|Response|null
     */
    public function connectCheckAction(Request $request, ClientRegistry $clientRegistry, GuardAuthenticatorHandler $guard, LoginFormAuthenticator $login)
    {
        /** @var FacebookClient $client */
        $client = $clientRegistry->getClient('facebook');

        /** @var FacebookUser $user */
        $infos = $client->fetchUser();
        if($infos != null) {
            $email = $infos->getEmail();
            $username = explode('@',$infos->getEmail())[0];
            $name = $infos->getName();

            if($this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $email]) == null) {
                $user = new User();
                $user->setEmail($email);
                $user->setUsername($username);
                $user->setNom($name);
                $user->setRoles(["ROLE_CLIENT"]);
                $user->setIsActive(1);
                $user->setPassword(uniqid());
                $manager = $this->getDoctrine()->getManager();
                $manager->persist($user);
                $manager->flush();
                return $guard->authenticateUserAndHandleSuccess($user,$request,$login,'default');
            } else {
                $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $email]);
                return $guard->authenticateUserAndHandleSuccess($user,$request,$login,'default');
            }
        }

        return $this->redirectToRoute('index_index');
    }
}