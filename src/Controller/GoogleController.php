<?php

namespace App\Controller;

use App\Entity\User;
use App\Security\LoginFormAuthenticator;
use Google_Client;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class GoogleController extends AbstractController
{
    /**
     * Link to this controller to start the "connect" process
     * @param ClientRegistry $clientRegistry
     *
     * @Route("/connect/google", name="connect_google_start")
     *
     * @return RedirectResponse
     */
    public function connectAction(ClientRegistry $clientRegistry)
    {
        return $clientRegistry
            ->getClient('google')
            ->redirect([
                'profile', 'email' // the scopes you want to access
            ]);
    }

    /**
     * After going to Google, you're redirected back here
     * because this is the "redirect_route" you configured
     * in config/packages/knpu_oauth2_client.yaml
     *
     * @param Request $request
     * @param ClientRegistry $clientRegistry
     *
     * @param GuardAuthenticatorHandler $guard
     * @param LoginFormAuthenticator $login
     * @return RedirectResponse|Response|null
     * @Route("/connect/google/check", name="connect_google_check")
     */
    public function connectCheckAction(Request $request, ClientRegistry $clientRegistry, GuardAuthenticatorHandler $guard, LoginFormAuthenticator $login)
    {
        $client = new Google_Client();
        $client->addScope("email");
        $client->setAccessType("offline");
        $client->setState("state_parameter_passthrough_value");
        $client->setRedirectUri("https://127.0.0.1:8000/connect/google/check");
        $client->setClientId($_ENV['OAUTH_GOOGLE_ID']);
        $client->setClientSecret($_ENV['OAUTH_GOOGLE_SECRET']);
        $client->setApprovalPrompt("consent");
        $client->setIncludeGrantedScopes(true);
        $client->fetchAccessTokenWithAuthCode($_GET['code']);
        $access_token = $client->getAccessToken();
        $client->setAccessToken($access_token);

        $infos = $client->verifyIdToken();
        if($infos != null) {
            $email = $infos["email"];
            $username = explode('@',$infos["email"])[0];
            $name = $infos["name"];

            if($this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $email]) == null) {
                $user = new User();
                $user->setEmail($email);
                $user->setUsername($username);
                $user->setNom($name);
                $user->setPassword(uniqid());
                $user->setRoles(["ROLE_CLIENT"]);
                $user->setIsActive(1);
                $manager = $this->getDoctrine()->getManager();
                $manager->persist($user);
                $manager->flush();
                return $guard->authenticateUserAndHandleSuccess($user,$request,$login,'default');
            } else {
                $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $email]);
                return $guard->authenticateUserAndHandleSuccess($user,$request,$login,'default');
            }
        }

        return $this->redirectToRoute('index_index');
    }
}