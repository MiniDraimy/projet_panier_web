<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index_index")
     * @return RedirectResponse|Response
     */
    public function index()
    {
        if($this->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('admin_produit_index');
        }
        if($this->isGranted('ROLE_CLIENT')) {
            return $this->redirectToRoute('client_panier_index');
        }
        return $this->render('accueil.html.twig');
    }

}