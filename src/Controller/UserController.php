<?php

namespace App\Controller;

use App\Entity\LigneCommande;
use App\Entity\User;
use App\Form\UserType;
use App\Repository\LigneCommandeRepository;
use App\Repository\TypeProduitRepository;
use App\Repository\UserRepository;
use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class UserController extends AbstractController
{
    private $passwordEncoder;
    private $token;
    private $csrfTokenManager;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, TokenGeneratorInterface $token, CsrfTokenManagerInterface $csrfTokenManager)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->token = $token;
        $this->csrfTokenManager = $csrfTokenManager;
    }

    /**
     * @Route("/user/add", name="user_add")
     * @param Request $request
     * @param MailerInterface $mailer
     * @return RedirectResponse|Response
     * @throws TransportExceptionInterface
     */
    public function addUser(Request $request, MailerInterface $mailer)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user,['method' => 'GET']);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $form->getData();
            $user->setIsActive(0)
                ->setRoles(["ROLE_CLIENT"])
                ->setPassword($this->passwordEncoder->encodePassword($user, $form->get('password')->getData()))
                ->setTokenMail($this->token->generateToken());
            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            $link = 'http://127.0.0.1:8000/login/'.$user->getTokenMail();

            $email = new Email();
            $email->from(new Address('alexis25.py@gmail.com', 'Support Contact'))
                ->to(new Address($user->getEmail()))
                ->subject("Account validation")
                ->text("Hello ".$user->getNom().",\n".
                "\n".
                "click here to validate and activate your account : ".$link);
            try {
                $mailer->send($email);
            } catch (TransportExceptionInterface $e) {
                throw $e;
            }

            return $this->redirectToRoute('index_index');
        }

        return $this->render('user/addUser.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/user/edit/{id}", name="user_edit", methods={"GET","PUT"})
     * @param Request $request
     * @param null $id
     * @return RedirectResponse|Response
     */
    public function editUser(Request $request, $id=null)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        if (!$user)  throw $this->createNotFoundException('No user found for id '.$id);
        $form = $this->createForm(UserType::class, $user, [
            'action' => $this->generateUrl('user_edit',['id'=>$id]),
            'method' => 'PUT'
        ]);
        $form->get('password')->setData($user->getPassword());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $form->getData();
            $user->setIsActive(0)
                ->setPassword($this->passwordEncoder->encodePassword($user, $form->get('password')->getData()))
                ->setTokenMail($this->token->generateToken());
            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('index_index');
        }

        return $this->render('user/editUser.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/user/delete", name="user_delete")
     * @param Request $request
     * @IsGranted("ROLE_ADMIN")
     * @return RedirectResponse
     */
    public function deleteUser(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $id= $request->request->get('id');
        $user = $entityManager->getRepository(User::class)->find($id);
        if (!$user)  throw $this->createNotFoundException('No user found for id '.$id);
        $this->addFlash('notice', 'User supprimé');
        $entityManager->remove($user);
        $entityManager->flush();
        return $this->redirectToRoute('index_index');
    }

    /**
     * @Route("/admin/showClients", name="all_clients")
     * @param UserRepository $userRepository
     * @IsGranted("ROLE_ADMIN")
     * @return Response
     */
    public function allClients(UserRepository $userRepository)
    {
        return $this->render('admin/client/showClients.html.twig', ['users' => $userRepository->findAll()]);
    }

    /**
     * @Route("/admin/showStats", name="admin_stats")
     * @param LigneCommandeRepository $ligneCommandeRepository
     * @return Response
     * @IsGranted("ROLE_ADMIN")
     */
    public function statsClients(LigneCommandeRepository $ligneCommandeRepository)
    {
        $CA = $ligneCommandeRepository->CA()[0][1];
        $moyenneParType = $ligneCommandeRepository->moyenneParType();
        $moyenneParClient = $ligneCommandeRepository->moyenneParClient()[0];

        $before = '2020-01-01';
        $after = '2020-12-31';

        $periode = $ligneCommandeRepository->NBcommandeTotal($before,$after)[0]['cmd'];

        return $this->render('admin/client/statsClients.html.twig', [
            'CA' => $CA,
            'types' => $moyenneParType,
            'clients' => $moyenneParClient,
            'commandes' => $periode,
            'before' => $before,
            'after' => $after
        ]);
    }
    /**
     * @Route("/admin/showStats/settings", name="admin_stats_periode")
     * @param LigneCommandeRepository $ligneCommandeRepository
     * @return Response
     * @IsGranted("ROLE_ADMIN")
     */
    public function setPeriode(LigneCommandeRepository $ligneCommandeRepository)
    {
        $CA = $ligneCommandeRepository->CA()[0][1];
        $moyenneParType = $ligneCommandeRepository->moyenneParType();
        $moyenneParClient = $ligneCommandeRepository->moyenneParClient()[0];

        if($_POST['before'] != null) $before = $_POST['before'];
        if($_POST['after'] != null) $after = $_POST['after'];

        if(empty($before) || empty($after)) {
            $before = '2020-01-01';
            $after  = '2020-12-31';
        }

        $periode = $ligneCommandeRepository->NBcommandeTotal($before,$after)[0]['cmd'];

        return $this->render('admin/client/statsClients.html.twig', [
            'CA' => $CA,
            'types' => $moyenneParType,
            'clients' => $moyenneParClient,
            'commandes' => $periode,
            'before' => $before,
            'after' => $after
        ]);
    }
}
