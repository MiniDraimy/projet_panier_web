<?php

namespace App\Controller;

use App\Repository\UserRepository;
use App\Security\LoginFormAuthenticator;
use LogicException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/login/{token}", name="app_login_token", methods={"GET"})
     * @param UserRepository $userRepository
     * @param Request $request
     * @param String|null $token
     * @param LoginFormAuthenticator $login
     * @param GuardAuthenticatorHandler $guard
     * @return Response
     */
    public function loginWithToken(UserRepository $userRepository,Request $request, String $token, LoginFormAuthenticator $login, GuardAuthenticatorHandler $guard): Response
    {
        if($userRepository->findToken($token) != null) {
            $user = $userRepository->find($userRepository->findToken($token)[0]);
            if($user->getTokenMail() == $token && $user->getIsActive()==0) {
                $user->setIsActive(1);
                $this->getDoctrine()->getManager()->persist($user);
                $this->getDoctrine()->getManager()->flush();
                $request->request->all();
                return $guard->authenticateUserAndHandleSuccess($user,$request,$login,'default');
            }
            return $this->redirectToRoute('index_index');
        }
        return $this->render('security/error.html.twig');
    }
}
