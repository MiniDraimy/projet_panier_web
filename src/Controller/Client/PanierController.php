<?php
namespace App\Controller\Client;

use App\Entity\Panier;
use App\Entity\Produit;
use App\Entity\TypeProduit;
use App\Entity\User;
use App\Repository\CommentaireRepository;
use App\Repository\PanierRepository;
use App\Repository\ProduitRepository;
use App\Repository\TypeProduitRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PanierController extends AbstractController
{
    /**
     * @Route("/client", name="client_panier_index")
     * @Route("/client/panierProduits/show", name="client_panier_showProduits")
     * @param CommentaireRepository $commentaireRepository
     * @param ProduitRepository $produitRepository
     * @param PanierRepository $panierRepository
     * @param TypeProduitRepository $typeProduitRepository
     * @return Response
     */
    public function showProduitPanier(CommentaireRepository $commentaireRepository, ProduitRepository $produitRepository, PanierRepository $panierRepository, TypeProduitRepository $typeProduitRepository)
    {
        $prix[0] = explode('.',$this->getDoctrine()->getRepository(Produit::class)->prixMin()[0][1])[0];
        $prix[1] = explode('.',$this->getDoctrine()->getRepository(Produit::class)->prixMax()[0][1])[0];

        return $this->render('client/boutique/panier_produit.html.twig',
            ['produits' => $produits = $produitRepository->findBy([], ['typeProduit' => 'ASC', 'stock' => 'ASC', 'prix' => 'ASC']),
                'panier' => $panier = $panierRepository->findAll(),
                'types' => $type = $typeProduitRepository->findAll(),
                'commentaires' => $commentaireRepository->findAll(),
                'prix' => $prix
        ]);
    }

    /**
     * @Route("/client/panierProduits/add/{id}", name="client_panier_addProduits")
     * @param PanierRepository $panierRepository
     * @param int|null $id
     * @return RedirectResponse
     */
    public function addProduitPanier(PanierRepository $panierRepository, int $id=null) {
        $manager = $this->getDoctrine()->getManager();
        $quantite = $_POST['quantite'];
        if(!is_numeric($quantite) || $quantite==0) {
            return $this->redirectToRoute('client_panier_showProduits');
        }

        $produit = $this->getDoctrine()->getRepository(Produit::class)->find($id);
        if($produit->getStock()<=$quantite) {
            $quantite = $produit->getStock();
            $produit->setStock(0);
        } else {
            $produit->setStock($produit->getStock() - $quantite);
        }

        $panier = new Panier();

        $panier->setProduit($this->getDoctrine()->getRepository(Produit::class)->find($id))
            ->setDateAchat(DateTime::createFromFormat('Y-m-d', date('Y-m-d')))
            ->setQuantite($quantite)
            ->setUser($this->getDoctrine()->getRepository(User::class)->find($this->getUser()->getId()));

        $error = 1;
        if($panierRepository->findAll() != null) {
            $oldPaniers = $panierRepository->findAll();
            foreach ($oldPaniers as $oldPanier) {
                if ($oldPanier->getProduit() === $panier->getProduit() && $oldPanier->getUser() === $panier->getUser()) {
                    $error = 0;
                    $oldPanier->setQuantite($quantite+$oldPanier->getQuantite());
                }
            }
        }
        if ($error == 1 && $panier->getQuantite() != 0) {
            $manager->persist($panier);
        }
        $manager->persist($produit);
        $manager->flush();

        return $this->redirectToRoute('client_panier_showProduits', ['panier' => $panier]);
    }

    /**
     * @Route("/client/panierProduits/delete/{id}", name="client_panier_deleteProduits")
     * @param int $id
     * @param PanierRepository $panierRepository
     * @param ProduitRepository $produitRepository
     * @return RedirectResponse
     */
    public function deleteProduitPanier(int $id, PanierRepository $panierRepository, ProduitRepository $produitRepository)
    {
        $manager = $this->getDoctrine()->getManager();
        $page = $_POST['page'];
        $panier = $panierRepository->find($id);
        $produit = $produitRepository->find($panier->getProduit());
        $produit->setStock($produit->getStock()+$panier->getQuantite());
        $manager->remove($panier);
        $manager->persist($produit);
        $manager->flush();

        return $this->redirectToRoute($page);
    }

    /**
     * @Route("/client/panierProduits/update", name="client_panier_updateProduits")
     * @return RedirectResponse
     */
    public function updateProduitPanier() {
        $manager = $this->getDoctrine()->getManager();
        $quantite = $_POST['qte'];
        $produitID = $_POST['produitID'];
        $id = $_POST['id'];
        $page = $_POST['page'];
        $produit = $this->getDoctrine()->getRepository(Produit::class)->find($produitID);
        $panier = $this->getDoctrine()->getRepository(Panier::class)->find($id);
        if ($produit->getStock()==0) {
            return $this->redirectToRoute($page, ['panier' => $panier]);
        }
        if($produit->getStock()>0 || $quantite == -1) {
            $produit->setStock($produit->getStock() - $quantite);
            $panier->setQuantite($panier->getQuantite() + $quantite);
        }

        $manager->persist($panier);
        $manager->persist($produit);

        $manager->flush();

        if($panier->getQuantite()==0) {
            $manager->remove($panier);
            $manager->flush();
        }
        return $this->redirectToRoute($page, ['panier' => $panier]);
    }

    /**
     * @Route("/client/panierProduits/filters", name="client_setFilters", methods={"GET", "POST"})
     * @param Request $request
     * @param PanierRepository $panierRepository
     * @param CommentaireRepository $commentaireRepository
     * @return RedirectResponse|Response
     */
    public function addFilters(PanierRepository $panierRepository, CommentaireRepository $commentaireRepository) {
        $manager = $this->getDoctrine()->getManager();

        $produits = $manager->getRepository(Produit::class)->findBy([], ["prix" => "ASC"]);
        $types = $manager->getRepository(TypeProduit::class)->findBy([], ["libelle" => "ASC"]);

        $category = [];
        $prices = [];

        if (!empty($_POST['TYPES'])) {
            $category = $_POST['TYPES'];
        }

        $min = $_POST["min"];
        $max = $_POST["max"];

        if(empty($min) && empty($max)) {
            $prices[0] = $this->getDoctrine()->getRepository(Produit::class)->prixMin();
            $prices[1] = $this->getDoctrine()->getRepository(Produit::class)->prixMax();
        } else {
            $prices[0] = $min;
            $prices[1] = $max;
        }

        $session = $this->get("session");
        $session->set("filter", array(
            "category" => $category,
            "prices" => $prices
        ));

        return $this->render("client/boutique/panier_produit.html.twig", [
            'produits' => $produits,
            'types' => $types,
            'prices' => $prices,
            'prix' => $prices,
            'panier' => $panierRepository->findAll(),
            'commentaires' => $commentaireRepository->findAll(),
        ]);
    }
}