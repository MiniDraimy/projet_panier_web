<?php

namespace App\Controller\Client;

use App\Entity\Commande;
use App\Entity\Etat;
use App\Entity\LigneCommande;
use App\Repository\CommandeRepository;
use App\Repository\LigneCommandeRepository;
use App\Repository\PanierRepository;
use App\Repository\ProduitRepository;
use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CommandeController extends AbstractController
{
    /**
     * @Route("/client/panier/show", name="client_detailsPanier")
     * @param PanierRepository $panierRepository
     * @param float|null $total
     * @return Response
     */
    public function showDetailsPanier(PanierRepository $panierRepository, float $total = null): Response
    {
        if ($panierRepository->findAll() != null) {
            $paniers = $panierRepository->findAll();
            foreach ($paniers as $panier) {
                if ($panier->getUser() == $this->getUser()) {
                    $total += $panier->getQuantite() * $panier->getProduit()->getPrix();
                }
            }
        } else {
            $total = null;
        }

        return $this->render('client/panier/panier.html.twig', [
            'panier' => $panier = $panierRepository->findAll(), 'total' => $total
        ]);
    }

    /**
     * @Route("/client/panier/delete", name="client_deletePanier")
     * @param PanierRepository $panierRepository
     * @return Response
     */
    public function deletePanier(PanierRepository $panierRepository) {
        $manager = $this->getDoctrine()->getManager();
        if ($panierRepository->findAll() != null) {
            $paniers = $panierRepository->findAll();
            foreach ($paniers as $panier) {
                if ($panier->getUser() == $this->getUser()) {
                    $panier->getProduit()->setStock($panier->getProduit()->getStock()+$panier->getQuantite());
                    $panier->getProduit()->setDisponible(1);
                    $manager->remove($panier);
                }
            }
            $manager->flush();
        }
        return $this->render('client/panier/panier.html.twig', [
            'panier' => $panier = $panierRepository->findAll(),'total' => $total=null
        ]);
    }

    /**
     * @Route("/client/panier/valider", name="client_validerPanier")
     * @param PanierRepository $panierRepository
     * @return Response
     */
    public function validerPanier(PanierRepository $panierRepository) {
        $manager = $this->getDoctrine()->getManager();
        if ($panierRepository->findAll() != null) {
            $paniers = $panierRepository->findAll();
            $commande = new Commande();
            $commande->setDate(DateTime::createFromFormat('Y-m-d - H:i:s',date('Y-m-d - H:i:s')))
                ->setUser($this->getUser())
                ->setEtat($this->getDoctrine()->getRepository(Etat::class)->find(1));
            $manager->persist($commande);
            $manager->flush();
            foreach ($paniers as $panier) {
                $ligneCommande = new LigneCommande();
                if ($panier->getUser() == $this->getUser()) {
                    $ligneCommande->setCommande($this->getDoctrine()->getRepository(Commande::class)->find($commande->getId()))
                        ->setQuantite($panier->getQuantite())
                        ->setPrix($panier->getProduit()->getPrix()*$panier->getQuantite())
                        ->setProduit($panier->getProduit());
                    $manager->persist($ligneCommande);
                    $manager->remove($panier);
                    $manager->flush();
                }
            }
            $manager->flush();
        }
        return $this->redirectToRoute('client_showCommande');
    }

    /**
     * @Route("/client/commande/show", name="client_showCommande")
     * @param ProduitRepository $produitRepository
     * @param CommandeRepository $commandeRepository
     * @return Response
     */
    public function showCommande(ProduitRepository $produitRepository, CommandeRepository $commandeRepository): Response
    {
        return $this->render('client/commande/commande.html.twig', [
            'commandes' => $commandeRepository->findAll(),
            'produit' => $produitRepository->findAll()
        ]);
    }

    /**
     * @Route("/client/detailsCommande/show/{id}", name="client_showDetailsCommande")
     * @param LigneCommandeRepository $ligneCommandeRepository
     * @param int $id |null
     * @return Response
     */
    public function showDetailsCommande(LigneCommandeRepository $ligneCommandeRepository, int $id): Response
    {
        $total = $ligneCommandeRepository->total($id)[0][1];
        return $this->render('client/commande/detailsCommande.html.twig', [
            'ligneCommandes' => $ligneCommandeRepository->findCommandeByID($id),
            'total' => $total
        ]);
    }

    /**
     * @Route("/admin/commande/show", name="admin_showCommande")
     * @param ProduitRepository $produitRepository
     * @param CommandeRepository $commandeRepository
     * @IsGranted("ROLE_ADMIN")
     * @return Response
     */
    public function adminShowCommande(ProduitRepository $produitRepository, CommandeRepository $commandeRepository): Response
    {
        return $this->render('admin/commande/commande.html.twig', [
            'commandes' => $commandeRepository->findAll(),
            'produit' => $produitRepository->findAll()
        ]);
    }

    /**
     * @Route("/admin/commande/changeEtat", name="admin_changeEtatCommande")
     * @param Request $request
     * @param ProduitRepository $produitRepository
     * @param CommandeRepository $commandeRepository
     * @IsGranted("ROLE_ADMIN")
     * @return Response
     */
    public function adminChangeEtatCommande(Request $request, ProduitRepository $produitRepository, CommandeRepository $commandeRepository): Response
    {
        $manager = $this->getDoctrine()->getManager();
        $id= $request->request->get('id');
        $commande = $this->getDoctrine()->getRepository(Commande::class)->find($id);
        $commande->setEtat($this->getDoctrine()->getRepository(Etat::class)->find(2));

        $manager->persist($commande);
        $manager->flush();

        return $this->render('admin/commande/commande.html.twig', [
            'commandes' => $commandeRepository->findAll(),
            'produit' => $produitRepository->findAll()
        ]);
    }

    /**
     * @Route("/admin/commande/delete", name="admin_deleteCommande")
     * @param Request $request
     * @param ProduitRepository $produitRepository
     * @param CommandeRepository $commandeRepository
     * @IsGranted("ROLE_ADMIN")
     * @return Response
     */
    public function adminEtatDeleteCommande(Request $request, ProduitRepository $produitRepository, CommandeRepository $commandeRepository): Response
    {
        $manager = $this->getDoctrine()->getManager();
        $id= $request->request->get('id');
        $commande = $this->getDoctrine()->getRepository(Commande::class)->find($id);
        $commande->setEtat($this->getDoctrine()->getRepository(Etat::class)->find(3));

        $manager->persist($commande);
        $manager->flush();

        return $this->render('admin/commande/commande.html.twig', [
            'commandes' => $commandeRepository->findAll(),
            'produit' => $produitRepository->findAll()
        ]);
    }

    /**
     * @Route("/admin/detailsCommande/show/{id}", name="admin_showDetailsCommande")
     * @param int|null $id
     * @param LigneCommandeRepository $ligneCommandeRepository
     * @IsGranted("ROLE_ADMIN")
     * @return Response
     */
    public function adminShowDetailsCommande(int $id, LigneCommandeRepository $ligneCommandeRepository): Response
    {
        $total = $ligneCommandeRepository->total($id)[0][1];

        return $this->render('admin/commande/detailsCommande.html.twig', [
            'ligneCommandes' => $ligneCommandeRepository->findCommandeByID($id),
            'total' => $total
        ]);
    }
}
