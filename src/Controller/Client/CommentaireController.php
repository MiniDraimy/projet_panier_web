<?php

namespace App\Controller\Client;

use App\Entity\Commentaire;
use App\Entity\Produit;
use App\Entity\User;
use App\Form\CommentaireType;
use App\Repository\CommentaireRepository;
use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CommentaireController extends AbstractController
{
    /**
     * @Route("/client/commentaire/show", name="client_commentaire_show")
     * @param CommentaireRepository $commentaireRepository
     * @return Response
     */
    public function index(CommentaireRepository $commentaireRepository): Response
    {
        return $this->render('client/commentaire/showCommentaire.html.twig', [
            'commentaires' => $commentaireRepository->findAll()
        ]);
    }

    /**
     * @Route("/client/commentaire/add/{id}", name="client_commentaire_add", methods={"GET"})
     * @param Request $request
     * @param int|null $id
     * @return RedirectResponse|Response
     */
    public function addCommentaire(Request $request, int $id=null)
    {
        $commentaire = new Commentaire();
        $form = $this->createForm(CommentaireType::class, $commentaire,['method' => 'GET']);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $form->getData();
            $commentaire->setProduit($this->getDoctrine()->getRepository(Produit::class)->find($id))
                ->setUser($this->getDoctrine()->getRepository(User::class)->find($this->getUser()))
                ->setDate(DateTime::createFromFormat('Y-m-d',date('Y-m-d')));
            $this->getDoctrine()->getManager()->persist($commentaire);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('notice', 'Commentaire ajouté');
            return $this->redirectToRoute('client_commentaire_show');
        }

        return $this->render('client/commentaire/addCommentaire.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/client/commentaire/edit/{id}", name="client_commentaire_edit", methods={"GET","PUT"})
     * @param Request $request
     * @param null $id
     * @return RedirectResponse|Response
     */
    public function editCommentaire(Request $request, $id=null)
    {
        $commentaire = $this->getDoctrine()->getRepository(Commentaire::class)->find($id);
        if (!$commentaire)  throw $this->createNotFoundException('No commentaire found for id '.$id);
        $form = $this->createForm(CommentaireType::class, $commentaire, [
            'action' => $this->generateUrl('client_commentaire_edit',['id'=>$id]),
            'method' => 'PUT'
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $form->getData();
            $commentaire->setDate(DateTime::createFromFormat('Y-m-d',date('Y-m-d')));
            $this->getDoctrine()->getManager()->persist($commentaire);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('notice', 'Commentaire modifié');
            return $this->redirectToRoute('client_commentaire_show');
        }

        return $this->render('client/commentaire/editCommentaire.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/commentaire/delete", name="admin_commentaire_delete")
     * @param Request $request
     * @IsGranted("ROLE_ADMIN")
     * @return RedirectResponse
     */
    public function deleteCommentaire(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $id= $request->request->get('id');
        $commentaire = $entityManager->getRepository(Commentaire::class)->find($id);
        if (!$commentaire)  throw $this->createNotFoundException('No commentaire found for id '.$id);
        $this->addFlash('notice', 'Commentaire supprimé');
        $entityManager->remove($commentaire);
        $entityManager->flush();
        return $this->redirectToRoute('admin_commentaire_show');
    }

    /**
     * @Route("/admin/commentaire/show", name="admin_commentaire_show")
     * @param CommentaireRepository $commentaireRepository
     * @IsGranted("ROLE_ADMIN")
     * @return Response
     */
    public function indexAdmin(CommentaireRepository $commentaireRepository): Response
    {
        return $this->render('admin/commentaire/showCommentaire.html.twig', [
            'commentaires' => $commentaireRepository->findAll()
        ]);
    }
}
